import xlrd
import sys
from PyQt5.QtWidgets import QApplication, QFileDialog


def average(filenames):
    avg=0
    for i in filenames:
        radius=0
        book = xlrd.open_workbook(i)
        first_sheet = book.sheet_by_index(0)
        print(first_sheet.cell_value(16, 1))
        #radius = radius + first_sheet.cell_value(7, 3)
        #radius = radius + first_sheet.cell_value(8, 3)
        #avg= avg + radius/3
    return(avg/(filenames.__len__()))

def loadFiles():
    filter = "xls (*.xls)"
    file_name = QFileDialog()
    file_name.setFileMode(QFileDialog.ExistingFiles)
    names, _ = file_name.getOpenFileNames(None, "Open files", "E:\\", filter)
    print(names)
    return(names)

if __name__ == "__main__":
    app = QApplication(sys.argv)
    average(loadFiles())

    sys.exit()