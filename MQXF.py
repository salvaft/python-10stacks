import xlrd
import sys
from PyQt5.QtWidgets import QApplication, QFileDialog


def localizeMQXF(filename):
    book = xlrd.open_workbook(filename)
    first_sheet = book.sheet_by_index(0)
    nsamples = 3
    cycles = 3
    paso_por_03 = 0


    'nsamples=input("Number of samples")'
    pressures = {}

    if nsamples >= 1:
        pressures['S1_UI'] = first_sheet.col_values(8)
        pressures['S1_UB'] = first_sheet.col_values(23)
    if nsamples >= 2:
        pressures['S2_UI'] = first_sheet.col_values(38)
        pressures['S2_UB'] = first_sheet.col_values(53)
    if nsamples == 3:
        pressures['S3_UI'] = first_sheet.col_values(68)
        pressures['S3_UB'] = first_sheet.col_values(83)

    for sample, numbers in pressures.items():
        starting_row = 6
        for cycle in range(cycles):
            for row, value in enumerate(numbers[starting_row:]):
                if value <3:
                    paso_por_03 = 1

                if paso_por_03 == 1 and value > 5 and (value - 5) < 0.1:
                    print(f'{cycle+1}{" "}{sample}{" "}{row+1+starting_row}{" "}{value}')#+1 Porque empieza en 0 la enumeracion
                    starting_row = row+starting_row
                    paso_por_03 = 0

                    break
                elif paso_por_03 == 1 and value > 5 and (value - 5) > 0.1:
                    print(f'{cycle+1}{" "}{sample}{" "}{row+1+starting_row}{" "}{"Demasiado gap entre el stress target y el primer valor por encima "}{5-value}')#+1 Porque empieza en 0 la enumeracion
                    starting_row = row+starting_row
                    paso_por_03 = 0

                    break

    return

if __name__ == "__main__":
    app = QApplication(sys.argv)
    fileName, _ = QFileDialog.getOpenFileName(None, "QFileDialog.getOpenFileName()", "",
                                                  "All Files (*);;Python Files (*.py)")
    print(fileName)
    localizeMQXF(fileName)
    sys.exit()
