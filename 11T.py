import xlrd
import sys
from PyQt5.QtWidgets import QFileDialog, QApplication

def localize(filename):
    book = xlrd.open_workbook(filename)
    first_sheet = book.sheet_by_index(0)
    nsamples = 3
    cycles = 3
    load_steps = [5, 10, 20, 30, 20,10, 5]


    'nsamples=input("Number of samples")'
    pressures = {}

    if nsamples >= 1:
            pressures['S1_UI'] = first_sheet.col_values(8)
            pressures['S1_UB'] = first_sheet.col_values(23)
    if nsamples >= 2:
            pressures['S2_UI'] = first_sheet.col_values(38)
            pressures['S2_UB'] = first_sheet.col_values(53)
    if nsamples == 3:
            pressures['S3_UI'] = first_sheet.col_values(68)
            pressures['S3_UB'] = first_sheet.col_values(83)

    for sample, numbers in pressures.items():
        starting_row = 6
        for n, step in enumerate(load_steps):
            if n <= 3:
                for row, value in enumerate(numbers[starting_row:]):
                    if value > step and (value - step) < 0.1:
                        print(f'{step}{" "}{sample}{" "}{row+1+starting_row}{" "}{value}') #+1 Porque empieza en 0 la enumeracion
                        starting_row = row+starting_row
                        break
                    elif value > step and (value - step) > 0.1:
                        print(f'{step}{" "}{sample}{" "}{row+1+starting_row}{" "}{"Demasiado gap entre el stress target y el primer valor por encima "}{value}')  # +1 Porque empieza en 0 la enumeracion
                        starting_row = row + starting_row
                        break
            if n > 3:
                for row, value in enumerate(numbers[starting_row:]):
                    if value < step:
                        if numbers[starting_row+row - 1] > step and (numbers[starting_row+row - 1]-step) < 0.1:
                            print(f'{step}{" "}{sample}{" "}{row+starting_row}{" "}{numbers[starting_row+row - 1]}')
                            starting_row = row + starting_row + 1  # +1 Porque empieza en 0 la enumeracion
                            break
                        elif numbers[starting_row+row - 1] > step and (numbers[starting_row+row - 1]-step) > 0.1:
                            print(f'{step}{" "}{sample}{" "}{row+starting_row}{" "}{"Demasiado gap entre el stress target y el primer valor por encima "}{numbers[starting_row+row - 1]}')
                            starting_row = row + starting_row + 1  # +1 Porque empieza en 0 la enumeracion
                            break


    return

if __name__ == "__main__":
    app = QApplication(sys.argv)
    filename, _ = QFileDialog.getOpenFileName(None, "QFileDialog.getOpenFileName()", "",
                                              "All Files (*);;Python Files (*.py)")
    localize(filename)